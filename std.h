#ifndef _STD_STD_H
#define _STD_STD_H

#ifdef __cplusplus
extern "C" {
#endif

#define _STD_DEPENDENCY_MISSING(x, y) "`" #x "` required for `" #y "` extension"

/*
 * #define LOG_WARN_ENABLED
 * #define LOG_INFO_ENABLED
 * #define _STD_RELEASE      // #define _STD_DEBUG
 * #define _STD_PLDETECT_H
 * #define _STD_TYPES_H
 * #define _STD_RAND_H
 * #define _STD_TIME_H
 * #define _STD_LOG_H
 * #define _STD_ASSERT_H
 * #define _STD_STRING_H
 * #include "std.h"
*/

#ifdef _STD_TYPES_H
/*
 *   u8l     u8f     u8
 *   u16l    u16f    u16
 *   u32l    u32f    u32
 *   u64l    u64f    u64
 *   s8l     s8f     s8
 *   s16l    s16f    s16
 *   s32l    s32f    s32
 *   s64l    s64f    s64
 *   f32     f64
 *   b8      b32
 *   true    false
*/

#include <stdbool.h>
#include <inttypes.h>

typedef void  u0;

typedef uint_least8_t u8l;
typedef uint_fast8_t  u8f;
typedef uint8_t       u8;

typedef uint_least16_t u16l;
typedef uint_fast16_t  u16f;
typedef uint16_t       u16;

typedef uint_least32_t u32l;
typedef uint_fast32_t  u32f;
typedef uint32_t       u32;

typedef uint_least64_t u64l;
typedef uint_fast64_t  u64f;
typedef uint64_t       u64;

typedef int_least8_t s8l;
typedef int_fast8_t  s8f;
typedef int8_t       s8;

typedef int_least16_t s16l;
typedef int_fast16_t  s16f;
typedef int16_t       s16;

typedef int_least32_t s32l;
typedef int_fast32_t  s32f;
typedef int32_t       s32;

typedef int_least64_t s64l;
typedef int_fast64_t  s64f;
typedef int64_t       s64;

typedef float  f32;
typedef double f64;

typedef u8  b8;
typedef u32 b32;
#else
#define u0  void
#define u8  unsigned char
#define u16 unsigned short
#define u32 unsigned int
#define u64 unsigned long long

#define s8  signed char
#define s16 signed short
#define s32 signed int
#define s64 signed long long

#define f32 float
#define f64 double

#define b8  u8
#define b32 u32
#endif /* _STD_TYPES_H */

/* simple platform detection layer
 * _STD_PLATFORM_WINDOWS
 * _STD_PLATFORM_WIN32
 * _STD_PLATFORM_WIN64
 * _STD_PLATFORM_APPLE
 * _STD_PLATFORM_IPHONE_SIMULATOR
 * _STD_PLATFORM_MACCATALYST
 * _STD_PLATFORM_IPHONE
 * _STD_PLATFORM_MAC
 * _STD_PLATFORM_ANDROID
 * _STD_PLATFORM_LINUX
 * _STD_PLATFORM_UNIX
 * _STD_PLATFORM_POSIX */
#ifdef _STD_PLDETECT_H
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
  //define something for Windows (32-bit and 64-bit, this part is common)
  #define _STD_PLATFORM_WINDOWS
  #ifdef _WIN64
    //define something for Windows (64-bit only)
    #define _STD_PLATFORM_WIN64
  #else
    //define something for Windows (32-bit only)
    #define _STD_PLATFORM_WIN32
  #endif
#elif __APPLE__
  #define _STD_PLATFORM_APPLE
  #include <TargetConditionals.h>
  #if TARGET_IPHONE_SIMULATOR
    // iOS, tvOS, or watchOS Simulator
    #define _STD_PLATFORM_IPHONE_SIMULATOR
  #elif TARGET_OS_MACCATALYST
    // Mac's Catalyst (ports iOS API into Mac, like UIKit).
    #define _STD_PLATFORM_MACCATALYST
  #elif TARGET_OS_IPHONE
    // iOS, tvOS, or watchOS device
    #define _STD_PLATFORM_IPHONE
  #elif TARGET_OS_MAC
    // Other kinds of Apple platforms
    #define _STD_PLATFORM_MAC
  #else
    #error "std.h {_STD_PLDETECT_H}: `Unknown Apple platform`"
  #endif
#elif __ANDROID__
  // Below __linux__ check should be enough to handle Android,
  // but something may be unique to Android.
  #define _STD_PLATFORM_ANDROID
  #define _STD_PLATFORM_UNIX
#elif __linux__
  // linux
  #define _STD_PLATFORM_LINUX
  #define _STD_PLATFORM_UNIX
#elif __unix__ // all unices not caught above
  // Unix
  #define _STD_PLATFORM_UNIX
#elif defined(_POSIX_VERSION)
  // POSIX
  #define _STD_PLATFORM_POSIX
  #define _STD_PLATFORM_UNIX
#else
  #error "std.h {_STD_PLDETECT_H}: `Unknown platform`"
#endif
#endif /* _STD_PLDETECT_H */
/* * * * * * * * * * * */

/* simple logger implementation
 * #define LOG_WARN_ENABLED
 * #define LOG_INFO_ENABLED
 * _FATAL("...")
 * _ERROR("...")
 * _WARN("...")
 * _INFO("...")
 * _DEBUG("...")
 * _TRACE("...") */
#ifdef _STD_LOG_H
#ifndef _STD_PLDETECT_H
#error DEPENDENCY_MISSING(_STD_PLDETECT_H, _STD_LOG_H)
#endif

#include <stdio.h>
#include <stdarg.h>

#ifndef _STD_RELEASE
#define LOG_DEBUG_ENABLED
#define LOG_TRACE_ENABLED
#endif

typedef enum{
  LOG_LEVEL_FATAL = 0,
  LOG_LEVEL_ERROR,
  LOG_LEVEL_WARN,
  LOG_LEVEL_INFO,
  LOG_LEVEL_DEBUG,
  LOG_LEVEL_TRACE,
} log_level;

#define _STD_LOG_FATAL_REPR "[/]"
#define _STD_LOG_ERROR_REPR "[-]"
#define _STD_LOG_WARN_REPR  "[!]"
#define _STD_LOG_INFO_REPR  "[#]"
#define _STD_LOG_DEBUG_REPR "[~]"
#define _STD_LOG_TRACE_REPR "[>]"

#ifdef _STD_PLATFORM_UNIX
typedef enum platform_color {
  _STD_COLOR_GREEN       = 32,
  _STD_COLOR_YELLOW      = 33,
  _STD_COLOR_RED         = 31,
  _STD_COLOR_BLUE        = 34,
  _STD_COLOR_WHITE       = 37,

  _STD_BACKGROUND_GREEN  = 42,
  _STD_BACKGROUND_YELLOW = 43,
  _STD_BACKGROUND_RED    = 41,
  _STD_BACKGROUND_BLUE   = 44,
  _STD_BACKGROUND_WHITE  = 47,
} platform_color;
#elif defined(_STD_PLATFORM_WINDOWS)
#include <windef.h>
#include <winbase.h>
#include <wincon.h>

typedef enum platform_color {
  _STD_COLOR_GREEN       = FOREGROUND_GREEN,
  _STD_COLOR_YELLOW      = (FOREGROUND_RED | FOREGROUND_GREEN),
  _STD_COLOR_RED         = FOREGROUND_RED,
  _STD_COLOR_BLUE        = FOREGROUND_BLUE,
  _STD_COLOR_WHITE       = (FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE),

  _STD_BACKGROUND_GREEN  = BACKGROUND_GREEN,
  _STD_BACKGROUND_YELLOW = (BACKGROUND_RED | BACKGROUND_GREEN),
  _STD_BACKGROUND_RED    = BACKGROUND_RED,
  _STD_BACKGROUND_BLUE   = BACKGROUND_BLUE,
  _STD_BACKGROUND_WHITE  = (BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE),
} platform_color;
#else
typedef enum platform_color {
  _STD_COLOR_GREEN       = 0,
  _STD_COLOR_YELLOW      = 0,
  _STD_COLOR_RED         = 0,
  _STD_COLOR_BLUE        = 0,
  _STD_COLOR_WHITE       = 0,

  _STD_BACKGROUND_GREEN  = 0,
  _STD_BACKGROUND_YELLOW = 0,
  _STD_BACKGROUND_RED    = 0,
  _STD_BACKGROUND_BLUE   = 0,
  _STD_BACKGROUND_WHITE  = 0,
} platform_color;
#endif

/* logs a fatal-level message  */ 
#ifndef _STD_FATAL
#define _STD_FATAL(message, ...) log_output(LOG_LEVEL_FATAL, message, ##__VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs an error-level message */ 
#ifndef _STD_ERROR
#define _STD_ERROR(message, ...) log_output(LOG_LEVEL_ERROR, message, ##__VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs a warning-level message  */ 
#ifndef _STD_WARN
#ifdef LOG_WARN_ENABLED
#define _STD_WARN(message, ...) log_output(LOG_LEVEL_WARN, message, ##__VA_ARGS__)
#else
#define _STD_WARN(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * * */

/* logs an info-level message  */ 
#ifndef _STD_INFO
#ifdef LOG_INFO_ENABLED
#define _STD_INFO(message, ...) log_output(LOG_LEVEL_INFO, message, ##__VA_ARGS__)
#else
#define _STD_INFO(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* logs a debug-level message  */ 
#ifndef _STD_DEBUG
#ifdef LOG_DEBUG_ENABLED
#define _STD_DEBUG(message, ...) log_output(LOG_LEVEL_DEBUG, message, ##__VA_ARGS__)
#else
#define _STD_DEBUG(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* logs a trace-level message  */ 
#ifndef _STD_TRACE
#ifdef LOG_TRACE_ENABLED
#define _STD_TRACE(message, ...) log_output(LOG_LEVEL_TRACE, message, ##__VA_ARGS__)
#else
#define _STD_TRACE(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* 
   "[/]: fatal-level   log output" 
   "[-]: error-level   log output"
   "[!]: warning-level log output"
   "[#]: info-level    log output"
   "[~]: debug-level   log output"
   "[>]: trace-level   log output"
*/

#ifndef _STD_PLDETECT_H
#error _STD_DEPENDENCY_MISSING(_STD_PLDETECT_H, _STD_LOGS_H)
#endif

#ifdef _STD_PLATFORM_UNIX
inline static u0 _cmd_color(u8 color, b8 bold){
  /* fatal, error, warn, info, debug, trace */
  printf("\033[%d;%dm", bold, color);
}

inline static u0 _cmd_blank(u0){
  printf("\033[0m");
}
#elif defined(_STD_PLATFORM_WINDOWS)
static b8 _color_init = false;

static HANDLE _h_console;
static WORD _saved_attributes;

inline static u0 _cmd_color(const u8 color, b8 bold) {
  if (!_color_init) {
    _h_console = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    GetConsoleScreenBufferInfo(_h_console, &consoleInfo);
    _saved_attributes = consoleInfo.wAttributes;
#ifdef _STD_TYPES_H
    _color_init = true;
#else
    _color_init = 1;
#endif
  }
  SetConsoleTextAttribute(_h_console, ((bold) ? (color | FOREGROUND_INTENSITY) : (color)));
}

inline static u0 _cmd_blank(u0) {
  SetConsoleTextAttribute(_h_console, _saved_attributes);
}
#else
inline static u0 _cmd_color(u8 color, b8 color){
  /* fatal, error, warn, info, debug, trace */
  return;
}

inline static u0 _cmd_blank(u0){
  return;
}
#endif

#include <stdlib.h>
#include <math.h>
#define NUM_LEN(x) ((fabs((f64)x) > 0) ?\
                   ((s32)floor(log10((fabs((f64)x))) * sizeof(char)) + sizeof(char)) : (sizeof(char)))

/* conversion from int to 
 * an ascii string with arbitrary base */
inline static char * itoa(s32 value, char * result, s8 base) {
  /* making sure the base is valid */
  if (base < 2 || base > 36) {
    *result = '\0';
    return result;
  }
  /* * * * * * * * * * * * * * * * */

  char * ptr = result, *ptr1 = result, tmp_char;
  s32 tmp_val;

  do {
    tmp_val = value;
    value /= base;
    *ptr++ =
      "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"
      [35 + (tmp_val - value * base)];
  } while (value);

  if (tmp_val < 0)
    *ptr++ = '-';
  *ptr-- = '\0';
  while (ptr1 < ptr) {
    tmp_char = *ptr;
    *ptr-- = *ptr1;
    *ptr1++ = tmp_char;
  }
  return result;
}
/* * * * * * * * * * * * * * * * * * * */

/* formatted coloring print function */
inline static u0 fcprint(FILE * stream, const char * message, ...) {
  /* NOTE: this is a workaround 
     for overwritten va_list on windows */
  __builtin_va_list args; 
  va_start(args, message);

  for (; *message; ++message) {
    switch (*message) {
      default:
        fputc(*message, stream);
        break;

      case '%':
        ++message;
        switch (*message) {
          case 'c': {
            char i = va_arg(args, s32);
            fputc(i, stream);
            break;
          }
          case 'd': {
            s32 i = va_arg(args, s32);
            u64 l = NUM_LEN(i);
            char res[l];
            itoa(i, res, 10);
            for (u64 j = 0; res[j] != '\0'; ++j) {
              fputc(res[j], stream);
            }
            break;
          }
          case 'f': {
            f64 i = va_arg(args, f64) * 1000000;
            u64 l = NUM_LEN(i);
            char res[l];
            itoa(i, res, 10);
            for (u64 j = 0; res[j] != '\0'; ++j) {
              if ((l - j) == 6) {
                fputc('.', stream);
              }
              fputc(res[j], stream);
            }
            break;
          }
          case 's': {
            const char * s = va_arg(args, char *);
            for (; *s; ++s) {
              fputc(*s, stream);
            }
            break;
          }
          case 'x': {
            s32 i = va_arg(args, s32);
            u64 l = NUM_LEN(i);
            char res[l];
            itoa(i, res, 16);
            for (u64 j = 0; res[j] != '\0'; ++j) {
              fputc(res[j], stream);
            }
            break;
          }
          case 'o': {
            s32 i = va_arg(args, s32);
            u64 l = NUM_LEN(i);
            char res[l];
            itoa(i, res, 8);
            for (u64 j = 0; res[j] != '\0'; ++j) {
              fputc(res[j], stream);
            }
            break;
          }
          case 'n': {
            switch (*(message + 1)) {
              default:
                _cmd_blank();
                break;
              case 'R':
                ++message;
                _cmd_color(_STD_COLOR_RED, false);
                break;
              case 'G':
                ++message;
                _cmd_color(_STD_COLOR_GREEN, false);
                break;
              case 'B':
                ++message;
                _cmd_color(_STD_COLOR_BLUE, false);
                break;
              case 'Y':
                ++message;
                _cmd_color(_STD_COLOR_YELLOW, false);
                break;
              case 'W':
                ++message;
                _cmd_color(_STD_COLOR_WHITE, false);
                break;
            }
            break;
          }
          case 'b': {
            switch (*(message + 1)) {
              default: {
                /* binary notation */
                s32 i = va_arg(args, s32);
                u64 l = NUM_LEN(i);
                char res[l];
                itoa(i, res, 2);
                for (u64 j = 0; res[j] != '\0'; ++j) {
                  fputc(res[j], stream);
                }
                break;
              }
              case 'R':
                ++message;
                _cmd_color(_STD_COLOR_RED, true);
                break;
              case 'G':
                ++message;
                _cmd_color(_STD_COLOR_GREEN, true);
                break;
              case 'B':
                ++message;
                _cmd_color(_STD_COLOR_BLUE, true);
                break;
              case 'Y':
                ++message;
                _cmd_color(_STD_COLOR_YELLOW, true);
                break;
              case 'W':
                ++message;
                _cmd_color(_STD_COLOR_WHITE, true);
                break;
            }
            break;
          }
          case '-': {
            switch (*(message + 1)) {
              default:
                break;
              case 's': {
                ++message;
                s32 i = va_arg(args, s32);
                char * s = va_arg(args, char*);
                for (s32 j = 0; s[j] != 0 && j < i; ++j) {
                  fputc(s[j], stream);
                }
                break;
              }
            }
            break;
          }
          case '+': {
            switch (*(message + 1)) {
              default:
                break;
              case 's': {
                ++message;
                s32 i = va_arg(args, s32);
                char * s = va_arg(args, char*);
                for (s32 p = 0; p < i; ++p) {
                  for (u64 j = 0; s[j] != 0; ++j) {
                    fputc(s[j], stream);
                  }
                }
                break;
              }
            }
            break;
          }
        }
        break;
    }
  }
  va_end(args);
}
/* * * * * * * * * * * * * * * * * * */

inline static u0 log_output(log_level level, const char * const message, ...){
  const char * level_repr[] = {
    [LOG_LEVEL_FATAL] = _STD_LOG_FATAL_REPR ": ",
    [LOG_LEVEL_ERROR] = _STD_LOG_ERROR_REPR ": ",
    [LOG_LEVEL_WARN]  = _STD_LOG_WARN_REPR  ": ",
    [LOG_LEVEL_INFO]  = _STD_LOG_INFO_REPR  ": ",
    [LOG_LEVEL_DEBUG] = _STD_LOG_DEBUG_REPR ": ",
    [LOG_LEVEL_TRACE] = _STD_LOG_TRACE_REPR ": "
  };

  const u8 map[] = {
    [LOG_LEVEL_FATAL] = _STD_BACKGROUND_RED,
    [LOG_LEVEL_ERROR] = _STD_COLOR_RED,
    [LOG_LEVEL_WARN]  = _STD_COLOR_YELLOW,
    [LOG_LEVEL_INFO]  = _STD_COLOR_GREEN,
    [LOG_LEVEL_DEBUG] = _STD_COLOR_BLUE,
    [LOG_LEVEL_TRACE] = _STD_BACKGROUND_WHITE,
  };

  /* NOTE: this is a workaround 
     for overwritten va_list on windows */
  __builtin_va_list args; 
  va_start(args, message);

  _cmd_color(map[level], (level < LOG_LEVEL_WARN));
  fprintf(stdout, "%s", level_repr[level]);
  vfprintf(stdout, message, args);
  _cmd_blank();
  va_end(args);
}
/* * * * * * * * * * * * * * * * */
#else
/* logs a fatal-level message  */ 
#ifndef _STD_FATAL
#define _STD_FATAL(message, ...) fprintf(stderr, _STD_LOG_FATAL_REPR ": %s\n", ##_VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs an error-level message */ 
#ifndef _STD_ERROR
#define _STD_ERROR(message, ...) fprintf(stderr, _STD_LOG_ERROR_REPR ": %s\n", ##_VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs a warning-level message  */ 
#ifndef _STD_WARN
#define _STD_WARN(message, ...) fprintf(stderr, _STD_LOG_WARN_REPR ": %s\n", ##_VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * * */

/* logs an info-level message  */ 
#ifndef _STD_INFO
#define _STD_INFO(message, ...) fprintf(stderr, _STD_LOG_INFO_REPR ": %s\n", ##_VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs a debug-level message  */ 
#ifndef _STD_DEBUG
#define _STD_DEBUG(message, ...) fprintf(stderr, _STD_LOG_DEBUG_REPR ": %s\n", ##_VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs a trace-level message  */ 
#ifndef _STD_TRACE
#define _STD_TRACE(message, ...) fprintf(stderr, _STD_LOG_TRACE_REPR ": %s\n", ##_VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */
#endif /* _STD_LOG_H */
/* * * * * * * * */

/* simple assertion implementation
 * _STD_ASSERT(1 != 2);
 * _STD_ASSERT_MSG(1 != 2, "Expression failure");
 * _STD_ASSERT_DEBUG(1 != 2);  */
#ifdef _STD_ASSERT_H
#ifndef _STD_LOG_H
#error _STD_DEPENDENCY_MISSING(_STD_LOG_H, _STD_ASSERT_H)
#endif

#define _STD_ASSERTIONS_ENABLED

#ifdef _STD_ASSERTIONS_ENABLED
#if _MSC_VER
#include <intrin.h>
#define debug_break() __debugbreak()
#else
#define debug_break() __builtin_trap()
#endif

inline static u0 report_assertion_failure(const char * const expression,
                                          const char * const message,
                                          const char * const file,
                                          s32 line) {
  log_output(LOG_LEVEL_FATAL,
             "Assertion Failure: %s, message: \"%s\", in file: %s, line %d",
             expression,   message,
             file,         line);
}

#define _STD_ASSERT(expr){                                           \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, "", __FILE__, __LINE__);       \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}

#define _STD_ASSERT_MSG(expr, message){                              \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, message, __FILE__, __LINE__);  \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}

#ifdef _STD_DEBUG
#define _STD_ASSERT_DEBUG(expr){                                     \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, "", __FILE__, __LINE__);       \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}
#else
#define _STD_ASSERT_DEBUG(expr)
#endif

#else
#define _STD_ASSERT(expr)
#define _STD_ASSERT_MSG(expr, message)
#define _STD_ASSERT_DEBUG(expr)
#endif

#endif /* _STD_ASSERT_H */
/* * * * * * * * * * * * * * * */

/* random number generation
 * _std_seed(seed);
 * _std_random();
 * _std_random_range();
 * basic LCG random number generator implementation*/
#ifdef _STD_RAND_H
static u32 seed = 1;

inline static u0 _std_seed(u32 newseed){
  seed = (unsigned)newseed & 0x7fffffffU;
}

/* basic lcg rand implementation */
inline static u32 _std_random(u0){
  seed = (seed * 1103515245U + 12345U) & 0x7fffffffU;
  return (u32)seed;
}

inline static u32 _std_random_range(u32 min, u32 max){
  return ((_std_random() % max) + min);
}
#endif /* _STD_RAND_H */
/* * * * * * * * * * * * * * * * * * * * * * * * * */

/* simple string + string_view implementation
 * string mystr = string_init("str");
 * string_append(&mystr, "abcdef");
 * string_free(&mystr);  */
#ifdef _STD_STRING_H

#include <stdlib.h>

#define STRING_INIT {NULL, 0, 0}
typedef struct _str {
  char * data;
  u64 length;
  u64 capacity;
} _str;

/* calculate string length in bytes  */
inline static u64 cstring_length(const char * s) {
  if (!s) {
    return 0;
  }

  const char * b = s;
  for (; *b; ++b);
  return (b - s);
}
/* * * * * * * * * * * * * * * * * * */

/* compare two cstrings and return length */
inline static u64 cstring_cmp(const char * a,
                              const char * b,
                              u64 length) {
  if (!a || !b) {
    return 0;
  }

  if (!length) {
    const u64 a_len = cstring_length(a);
    const u64 b_len = cstring_length(b);
    length = ((a_len > b_len) ? b_len : a_len);
  }

  u64 i = 0;
  for (; i < length; ++i) {
    if (a[i] != b[i]) {
      break;
    }
  }
  return i;
}
/* * * * * * * * * * * * * * * * * * * *  */

/* return next occurrence of delimiter */
inline static u64 cstring_find(char * s, char delim) {
  if (!s) {
    return 0;
  }

  char * b = s;
  for (; *b; ++b) {
    if (*b == delim) {
      break;
    }
  }
  return (b - s);
}
/* * * * * * * * * * * * * * * * * * * */

/* append to cstring */
inline static u0 cstring_append(char * const out, u64 out_len,
                                const char * const in, u64 in_len,
                                const u64 max) {
  if (!in_len) {
    in_len = cstring_length(in);
  }

  if (!out_len) {
    out_len = cstring_length(out);
  }

  u64 i = out_len;
  for (; i < max &&
         i < (out_len + in_len) &&
         in[i - out_len] != '\0'
       ; ++i) {
    out[i] = in[i - out_len];
  }
  out[i] = '\0';
}
/* * * * * * * * * * */

/* initialize a string */
inline static u0 str_append(_str * string,
                            const char * s,
                            u64 s_len) {
  if (!s_len) {
    s_len = cstring_length(s);
  }

  string->length    += s_len;
  string->capacity   = string->length + sizeof(char);
  string->data       = (char *)realloc(string->data, string->capacity);
  if (!string->data) {
    _STD_WARN("Failed allocating %d bytes on `string_append()` call\n", string->capacity);
    return;
  }

  cstring_append(string->data,
                 string->length - s_len,
                 s, s_len, string->capacity);
}
/* * * * * * * * * * * */

/* free a string */
inline static u0 str_free(_str * string) {
  if (string->data) {
    string->length   = 0;
    string->capacity = 0;
    free(string->data);
  }
}
/* * * * * * * * */

#define STRING_VIEW_INIT {NULL, 0}
typedef struct _strv {
  char * data;
  u64 length;
} _strv;

/* initialize a string_view  */
inline static _strv string_view(char * const s,
                               u64 s_len) {
  if (!s_len) {
    s_len = cstring_length(s);
  }

  _strv str = {
    .length = s_len,
    .data = s
  };
  return str;
}
/* * * * * * * * * * * * * * */

/* create substring and return it as an _strv  */
inline static _strv string_view_substr(char * src, const u64 offset, const u64 len) {
  if (!src) {
    return (_strv){NULL};
  }

  const u64 src_len = cstring_length(src);
  if (src_len < (offset + len)) {
    return (_strv){NULL};
  }

  _strv str = {
    .length = len,
    .data = (src + offset),
  };
  str.data[len] = '\0';
  return str;
}
/* * * * * * * * * * * * * * * * * * * * * * * */

/* read file and return its contents and size  */
inline static char * cstring_read_file(const char * file_path, u64 * length) {
  FILE * f = fopen(file_path, "rb");
  if (f) {
    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);

    if (*length) {
      /* refuse to load a file with length larger than 1 GiB */
      if (*length > 1073741824) {
        _STD_WARN("File `%s` too large\n", file_path);
        fclose(f);
        return (char *)NULL;
      }

      char * contents = (char *)malloc(*length + sizeof(char));
      u64 read_length = fread(contents, 1, *length, f);

      if (*length != read_length) {
        _STD_WARN("Failed reading file `%s`\n", file_path);
        free(contents);
        fclose(f);
        return (char *)NULL;
      }
      fclose(f);
      contents[*length] = '\0';
      return contents;
    }
  } else {
    _STD_WARN("File `%s` does not exist\n", file_path);
    return (char *)NULL;
  }
}
/* * * * * * * * * * * * * * * * * * * * * * * */

#endif /* _STD_STRING_H */
/* * * * * * * * * * * * */

/* simple time management
 * _std_sleep(2000ms)
 * _std_get_time() */
#ifdef _STD_TIME_H
#ifndef _STD_PLDETECT_H
#error _STD_DEPENDENCY_MISSING(_STD_PLDETECT_H, _STD_TIME_H)
#endif

#ifdef _STD_PLATFORM_UNIX
#if _POSIX_C_SOURCE >= 199309L
#include <time.h> /* nanosleep() */
#else
#include <unistd.h> /* usleep() */
#endif

#include <sys/time.h>

inline static f64 _std_get_time(u0){
  struct timespec now;
#ifdef CLOCK_MONOTONIC
  if (0 != clock_gettime(CLOCK_MONOTONIC, &now))
#endif
    clock_gettime(CLOCK_REALTIME, &now);
  return now.tv_sec + (now.tv_nsec * 0.000000001);
}

inline static u0 _std_sleep(u64 ms){
#if _POSIX_C_SOURCE >= 199309L
  struct timespec ts;
  ts.tv_sec = ms * 0.001;
  ts.tv_nsec = (ms % 1000) * 1000 * 1000;
  nanosleep(&ts, 0);
#else
  if (ms >= 1000){
    sleep(ms * 0.001);
  }
  usleep((ms % 1000) * 1000);
#endif
}
#elif defined(_STD_PLATFORM_WINDOWS)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

/* time requirements */
static f64 clock_frequency;
static LARGE_INTEGER start_time;
/* * * * * * * * * * */

static b8 _clock_init = false;

inline static f64 _std_get_time(u0){
  if (!_clock_init) {
    /* time requirements */
    LARGE_INTEGER frequency;
    QueryPerformanceFrequency(&frequency);
    clock_frequency = 1.0f / (f64)frequency.QuadPart;
    QueryPerformanceCounter(&start_time);
    /* * * * * * * * * * */
#ifdef _STD_TYPES_H
    _clock_init = true;
#else
    _clock_init = 1;
#endif
  }
  LARGE_INTEGER now_time;
  QueryPerformanceCounter(&now_time);
  return (f64)now_time.QuadPart * clock_frequency;
}

inline static u0 _std_sleep(u64 ms){
  Sleep(ms);
}
#else
#error "Incompatible platform with _STD_TIME_H"
#endif
#endif /* _STD_TIME_H */
/* * * * * * * * * */

#ifndef _STD_TYPES_H

#undef u0
#undef u8
#undef u16
#undef u32
#undef u64

#undef s8 
#undef s16
#undef s32
#undef s64

#undef f32
#undef f64

#undef b8
#undef b32

#endif

#ifndef _STD_LOG_H

#undef _STD_FATAL
#undef _STD_ERROR
#undef _STD_WARN
#undef _STD_INFO
#undef _STD_DEBUG
#undef _STD_TRACE

#endif

#undef _STD_DEPENDENCY_MISSING

#ifdef __cplusplus
}
#endif

#endif /* _STD_STD_H */
